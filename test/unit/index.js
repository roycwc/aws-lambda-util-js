const Upload = require('../../src/upload');
const assert = require('assert');
const sinon = require('sinon');
const AwsMock = require('aws-sdk-mock');
const fs = require('fs');

describe('Upload function', ()=>{
	
	it('should read given input argument', ()=>{
		let arg1 = 'hi1'
		let arg2 = 'hi2'
		let arg3 = 'hi3'
		let arg4 = 'hi4'
    
		let upload2 = new Upload([arg1,arg2,arg3,arg4])
		assert(upload2.src == arg1 && upload2.dest == arg2 && upload2.lambdaName == arg3, upload2.lambdaRole == arg4)
	})
	
	it('should zip (SRC) and upload to (DEST) and used by (LAMBDA)', (done)=>{
		after(()=>{
			fs.unlinkSync('./hello.zip')
		})
		
		AwsMock.mock('S3','upload',(params, callback)=>{
			assert(params.Bucket=='swiftcam20171023testdest' && params.Key=='codes/hello.zip')
			callback(null, {ETag:'myetag'})
		})
		
		let lambdaFunctionConfig = {
			'Code':{
				'RepositoryType':'S3',
				'Location':'s3://swiftcam20171023testdest/codes/hello.zip'
			},
			'Configuration':{
				'FunctionName': 'hello-test',
				'Version': '$LATEST',
				'MemorySize': 128,
				'Role': 'arn:aws:iam::1234567890:role/myrole',
				'Handler': 'index.handler',
				'Timeout': 3,
				'Runtime': 'nodejs6.10'
			}
		}
		
		AwsMock.mock('Lambda','getFunction',(params, callback)=>{
			assert(params.FunctionName=='hello-test')
			callback(null, lambdaFunctionConfig.Configuration)
		})

		let upload = new Upload(['test/testsrc','s3://swiftcam20171023testdest/codes/hello.zip','hello-test','arn:aws:iam::1234567890:role/myrole'])
		upload.start().then(uploadResult=>{
			assert(
				uploadResult.configuration.FunctionName=='hello-test' &&
				uploadResult.uploadResult.ETag=='myetag'
			)
			done()
		})
	})
})