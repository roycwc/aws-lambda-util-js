const archive = require('archiver')('zip')
const fs = require('fs');
const AWS = require('aws-sdk');
const dateformat = require('dateformat');

class Upload{
	constructor(args){
		if (args.length < 3) throw 'Not enough input argument. See `aws-lambda-util-js help`'
		this.src = args[0]
		this.dest = args[1]
		this.lambdaName = args[2]
		this.lambdaRole = args[3]
		
		let urlPcs = this.dest.replace('s3://','').split('/')
		this.bucket = urlPcs[0]
		this.filename = urlPcs[urlPcs.length-1]
		this.key = urlPcs.slice(1).join('/')
		this.cwd = process.cwd()
		this.destZip = this.cwd+'/'+this.filename
	}
	start(){
		return new Promise((resolve)=>{
			const destOutput = fs.createWriteStream(this.destZip);
			destOutput.on('close',_=>{
				resolve()
			})
			archive.pipe(destOutput)
			archive.directory(this.cwd+'/'+this.src+'/', false);
			archive.finalize()
		}).then(()=>{
			const s3 = new AWS.S3();
			const lambda = new AWS.Lambda();
			let uploadInputSteam = fs.createReadStream(this.destZip)
			
			return s3.upload({
				Bucket:this.bucket, 
				Key:this.key, 
				ContentType:'application/zip',
				Body:uploadInputSteam
			}).promise().then(uploadResult=>{
				return lambda.getFunction({
					FunctionName: this.lambdaName,
				}).promise().then(existingFunction=>{
					return lambda.updateFunctionCode({
						FunctionName: this.lambdaName, 
						Publish: true, 
						S3Bucket:this.bucket,
						S3Key:this.key
					}).promise().then((updatedConfiguration)=>{
						return {
							configuration:updatedConfiguration,
							uploadResult
						}
					})
				},err=>{
					return lambda.createFunction({
						Code:{
							S3Bucket:this.bucket,
							S3Key:this.key
						},
					  FunctionName: this.lambdaName, 
					  Handler: "index.handler",
					  Publish: true, 
					  Role: this.lambdaRole,
					  Runtime: "nodejs6.10"
					}).promise().then(createdFunction=>{
						return {
							configuration:createdFunction,
							uploadResult
						}
					})
				})
			})
		})
	}
}

module.exports = Upload;