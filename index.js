#!/usr/bin/env node
const Upload = require('./src/upload');
const argv = require('yargs')
	.demandCommand(1)
	.usage('Usage: $0 <command> [options]')
	.command('upload', 'Upload local folder to Cloud Object storage')
	.example('$0 upload -s ./src -d s3://mys3/code/src.zip -n mylambda -r arn:aws:iam::1234567890:role/my-lambda-role')
	.alias('s', 'src')
	.alias('d', 'dest')
	.alias('n', 'lambda-name')
	.alias('r', 'lambda-role')
	.describe('s', 'Local source folder to be uploaded (eg. ./src)')
	.describe('d', 'Remote Cloud Object storage path (eg. s3://mys3/code/src.zip)')
	.describe('n', 'Lambda function name to be created / updated (eg. mylambda)')
	.describe('r', 'Execution role of your lambda function (eg. arn:aws:iam::1234567890:role/my-lambda-role)')
  .demandOption(['s','d','n'])
	.argv


if (argv._[0]=='upload'){
	let uploader = new Upload([argv.s, argv.d, argv.n, argv.r])
	uploader.start()
}
